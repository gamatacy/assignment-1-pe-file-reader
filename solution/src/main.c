/// @file 
/// @brief Main application file

#include <pe_utils.h>
#include <stdio.h>

/// Application name string
#define APP_NAME "section-extractor"

/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *f)
{
  fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char** argv)
{
  if (argc != 4) {
        return 1;
    }

  FILE *in; 
  FILE *out;
  char *section_name = argv[2];

  if ((in = fopen(argv[1], "rb")) == NULL) return 1;
  if ((out = fopen(argv[3], "wb")) == NULL) return 1;

  struct PEFile *pe_file = malloc(sizeof(struct PEFile));

  read_pe_file(in, pe_file);
  print_sections(in, out, pe_file, section_name);

  free(pe_file->section_headers);
  free(pe_file);
  fclose(in);
  fclose(out);

  return 0;
}
