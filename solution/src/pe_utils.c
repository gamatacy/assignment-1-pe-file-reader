/// @file 
/// @brief Contains PE functions realisations

#include "pe_utils.h"
#include <string.h>



/** 
 * @brief Reading PE file 
 * @param in PE file 
 * @param pe_file PEFile structure
*/
void read_pe_file(FILE *in, struct PEFile *pe_file){
    //Skipping 0x3c (main) offset
    fseek(in, MAIN_OFFSET, SEEK_SET);
    
    uint32_t effective_offset;
    fread(&effective_offset, sizeof(effective_offset), 1, in);
    fseek(in, (long) effective_offset, SEEK_SET);  

    fread(&pe_file->magic, sizeof(pe_file->magic), 1, in);
    fread(&pe_file->header, sizeof(pe_file->header), 1, in);

    pe_file->section_headers = malloc(pe_file->header.NumberOfSections * sizeof(struct SectionHeader));

    //Skipping optional header
    fseek(in, pe_file->header.SizeOfOptionalHeader, SEEK_CUR);

    fread(pe_file->section_headers, sizeof(struct SectionHeader), pe_file->header.NumberOfSections, in);
}

/** 
 * @brief Prints section if name matched
 * @param in PE file 
 * @param out File to write section
 * @param pe_file PEFile structure
 * @param struct_name Name of structure to print
*/
void print_sections(FILE *in, FILE *out, struct PEFile *pe_file, char *struct_name){
    for (size_t i = 0; i < pe_file->header.NumberOfSections; i++){

        if ( strcmp((char*)pe_file->section_headers[i].Name, struct_name) == 0){

            fseek(in, pe_file->section_headers[i].PointerToRawData, SEEK_SET);

            char *raw_data = malloc(pe_file->section_headers[i].SizeOfRawData);
            fread(raw_data, pe_file->section_headers[i].SizeOfRawData, 1, in);
            fwrite(raw_data, pe_file->section_headers[i].SizeOfRawData, 1, out);
            free(raw_data);

        }
        
    }

}

